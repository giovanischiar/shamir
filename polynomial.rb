class Polynomial
	def initialize terms
		@terms = terms
	end

	#encontra o y baseado no array dos coeficientes do polinômio
	def find_y x, p
		y = 0
		@terms.each_with_index do |a, i|
			y += a * x ** i
		end
		return y % p
	end
end