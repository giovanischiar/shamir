require './pair'

class NewtonPolynomial
	def initialize points
		@points = points
	end

	#baseando-se nos pontos, encontra-se o y 
	#usando o polinômio de Newton gerado
	def find_y_in x, p
		c = get_coefficients
		result = 0
		x_points = get_x_list
		for i in 0...x_points.size
			xi = 1  
			for j in 1..i
				xi *= x - x_points[j-1]
			end
			result += c[i] * xi
		end
		return result % p
	end

	#Método descrito para encontrar os polinômios de Newton
	def get_coefficients
		ys = get_y_list
		x = get_x_list
		final = @points.size-1
		for i in 1..final
			diffy = sub ys[i..final],  ys[(i-1)..(final-1)]
			diffx = sub x[i..final], x[0..((final - i) + 1)]
			ys[i...@points.size] = div diffy, diffx
		end
		return ys
	end

	#divide-se cada elemento de a por b
	def div a, b
		result = []
		a.each_with_index do |ai, i|
			result.push ai/b[i].to_f
		end
		return result
	end

	#subtraíndo cada elemento de a por cada elemento de b
	def sub a, b
		result = []
		a.each_with_index do |ai, i|
			result.push ai-b[i]
		end
		return result
	end

	#pega uma lista de todos os x elementos
	def get_x_list
		x = []
		@points.each do |p|
			x.push(p.a)
		end
		return x
	end

	#pega uma lista de todos os y elementos
	def get_y_list
		y = []
		@points.each do |p|
			y.push(p.b)
		end
		return y
	end
end