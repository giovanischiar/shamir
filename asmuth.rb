require "prime"
require "./crt"

class Asmuth
	def initialize secret, k
		@secret = secret
		@k = k
		@crt = CRT.new
	end

	#primeiro, pegamos um array de coprimos de tamanho n+1
	#então verificamos as propriedades desse array que precisa
	#para se construir os shares. Após isso simplesmente escolhemos
	#um alfa que seja de acordo com o algoritmo.
	#após isso os shares são gerados usando o valor de y
	def get_shares n
		@m = get_primes @secret, 1
		get_primes(3*@m[0], n).each do |prime|
			@m.push prime
		end
		mi = @m[1..@k].reduce(1, :*)
		mk = @m[(n-@k+2)..n].reduce(@m[0], :*)
		if mi <= mk
			return 'problem'
		end

		a = rand 1..((mi - @secret)/@m[0])
		y = @secret + a * @m[0]

		shares = []

		@m[1..n].each do |x|
			shares.push(Pair.new(y % x, x))
		end
	
		return shares
	end

	#reconstroi do segredo usando o algoritmo de asmuth bloom
	#baseado no teorema chinês do resto
	def get_secret shares
		m = get_y_list shares
		ms = m.reduce(1, :*)
		s = @crt.calc m, shares
		secret = (s % ms) % @m[0]
	end

	#pega uma lista de todos os y elementos
	def get_y_list points
		y = []
		points.each do |p|
			y.push(p.b)
		end
		return y
	end

	#pegamos os n primos maiores que start
	def get_primes start, n
		primes = []
		i = start+1
		while primes.size < n do
			if Prime.prime? i
				primes.push i
			end
			i+=1
		end
		return primes
	end
end