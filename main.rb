require './finite-field'
require './shamir'
require "./asmuth"

if __FILE__ == $0
	puts "Shamir"
	gf = GF.new 1613, 1
	s = Shamir.new gf, 1234, 3
	points = s.get_points 6
	puts(points)
	puts(s.get_secret points[0..2])
	puts '-' * 100
	puts "Asmuth"
	a = Asmuth.new 5234, 3
	shares = a.get_shares 4
	puts(shares)
	puts(a.get_secret shares[0..2])
end