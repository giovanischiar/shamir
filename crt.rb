class CRT
	#resolve o teorema chines do resto. 
	#Encontra solução única do sistema de congruencias
	def calc m, shares
		ms = m.reduce(1, :*)
		i = []
		m.each do |prime|
			mm = ms/prime 
			e = ee(prime, mm)
			i.push e[2] * mm
		end
		s = 0
		shares.each_with_index do |share, index|
			s += share.a * i[index] 
		end
		return s
	end

	def ee(a, b)
		extended_euclidian(a, b, 1, 0, 0, 1)
	end

	#pega a identidade de bezout e o mdc de a e b
	def extended_euclidian a, b, xs1, xl1, xs2, xl2
		if a % b == 0
			return [b, xl1, xl2]
		end
		x1 = xs1 - xl1 * (a/b)
		x2 = xs2 - xl2 * (a/b)
		return extended_euclidian(b, (a%b), xl1, x1, xl2, x2);
	end
end