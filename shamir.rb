require './finite-field'
require './pair'
require './newton-polynomial'
require './polynomial'

class Shamir
	def initialize gf, secret, k
		@gf = gf
		r = Random.new Time.now.to_i
		a = Array.new(k-1) { |i| r.rand @gf.p }
		a.unshift secret
		@polynomial = Polynomial.new a
	end

	#gerador dos pontos de um polinômio em cima de gf para o algoritmo
	def get_points n
		points = []
		for i in 1..n
			points.push(Pair.new i, (@polynomial.find_y i, @gf.p))
		end
		return points
	end

	#usando o polinômio de newton com os pontos que foram gerados
	#no algoritmo da geração dos pontos
	def get_secret points
		newton = NewtonPolynomial.new points
		newton.find_y_in 0, @gf.p
	end
end
